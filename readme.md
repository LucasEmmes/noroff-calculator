## Verbal calculator

---
### How to build:  
First, build the calculator with  
`$ g++ src/main.cpp -o calculator.out`  
Then, run it with  
`$ ./calculator.out`

---
### How to use
The program will prompt you for your calculation. Simply write it out in english words.  
Your input will be parsed, and a result put on screen afterwards.  

Numbers must be **positive** and **below 1000 (one thousand)**.  
The list of possible operations include:  
1. **plus** (addition)  
    e.g. *"nine plus ten"*
2. **minus** (subtraction)  
    e.g. *"fifty minus eight"*
3. **times** (multiplication)  
    e.g. *"seven hundred seventy seven times seven hundred seventy seven"*
4. **over** (division)  
    e.g. *"seventeen over zero"* (please don't do this)
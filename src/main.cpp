#include <iostream>
#include <string.h>
#include <algorithm>
#include <vector>

// Declare vectors to look up the different token values
// The PASS tokens are spacers, such that each string is in the index of its value (or value/10 for tens)
const std::vector<std::string> tens = {"PASS", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
const std::vector<std::string> singles = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "PASS", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
const std::vector<std::string> operators = {"plus", "minus", "times", "over"};


// Function to check whether an string is contained in a vector
// Used to check if our token is tens / singles / operators
bool is_in(const std::string& value, const std::vector<std::string>& vector) {
    auto found = std::find(vector.begin(), vector.end(), value);
    return found != vector.end();
}


// Take user input and tokenize
std::vector<std::string> get_input() {
    // Prompt user
    std::cout << "Enter calculation: ";

    // Get input from use
    char raw_command_string[64];
    std::cin.getline(raw_command_string, sizeof(raw_command_string));
 
    // Split up string into tokens
    std::vector<std::string> tokens;
    char* token = strtok(raw_command_string, " ");
    while (token != NULL) {
        tokens.push_back(std::string(token));
        token = strtok(NULL, " ");
    }

    return tokens;
}

float parse_tokens(const std::vector<std::string>& tokens) {
    // a and b will store the first and second number (not token) respectively,
    // while op will store the type of operation the user wants
    float a = 0;
    float b = 0;
    int op = -1;

    // In order to make the code more compact, I parse the tokens into the pointer n,
    // which starts out pointing to a, but switches to b once the operator is found 
    float *n = &a;

    for (std::string token : tokens) {
        // Check for operators
        if (is_in(token, operators)) {
            // Find out which operator
            for (int i = 0; i < operators.size(); i++){
                if (token == operators[i]) {op = i;}
            }
            // Stop parsing a and switch to b
            n = &b;
            continue;
        }

        // Check for tens
        if (is_in(token, tens)) {
            for (int i = 0; i < tens.size(); i++)
            {
                if (token == tens[i]) {
                    // Once found, add in 10 times the index (therefore the PASS elements)
                    *n += i*10;
                }
            }
            continue;
        }

        // Check for singles (also encompasses teens)
        if (is_in(token, singles)) {
            for (int i = 0; i < singles.size(); i++)
            {
                if (token == singles[i]) {
                    // Once found, add in the index (as each string has the same index as it represents)
                    *n += i;
                }
            }
            continue;
        }

        if (token == "hundred") {
            // In case we find this token, we can assume the previous token was in the singles (e.g. TWO hundred)
            // Therefore, we can simply multiply n by 100 to get the correct value
            *n *= 100;
            continue;
        }

        // In case the token cannot be found, we throw an error
        std::cout << "Could not parse command: " << token << std::endl;
        throw "std::invalid_argument";
    }

    // Calculate the result based on the given operator
    float result;
    switch (op)
    {
    case 0:
        result = a + b;
        break;
    
    case 1:
        result = a - b;
        break;
        
    case 2:
        result = a * b;
        break;
        
    case 3:
        result = a / b;
        break;

    default:
        break;
    }

    return result;
}


int main(int argc, char const *argv[])
{
    float result;
    try
    {
        std::vector<std::string> tokens = get_input();    
        result = parse_tokens(tokens);
    }
    catch(const std::string error)
    {
        std::cout << error << std::endl;
        return 1;
    }
    
    // Print out result :)
    std::cout << "The result is: " << result << std::endl;

    return 0;
}
